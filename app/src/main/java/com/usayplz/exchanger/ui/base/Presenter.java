package com.usayplz.exchanger.ui.base;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public interface Presenter<V extends MvpView> {
    void attachView(V mvpView);
    void detachView();
}
