package com.usayplz.exchanger.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.usayplz.exchanger.App;
import com.usayplz.exchanger.injection.component.ActivityComponent;
import com.usayplz.exchanger.injection.component.ConfigPersistentComponent;
import com.usayplz.exchanger.injection.component.DaggerConfigPersistentComponent;
import com.usayplz.exchanger.injection.module.ActivityModule;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public class BaseActivity extends AppCompatActivity {
    private static final String KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID";
    private static final AtomicLong NEXT_ID = new AtomicLong(0);

    private static final Map<Long, ConfigPersistentComponent> componentsMap = new HashMap<>();
    private ActivityComponent activityComponent;
    private Long activityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityId = savedInstanceState != null ? savedInstanceState.getLong(KEY_ACTIVITY_ID) : NEXT_ID.getAndIncrement();

        ConfigPersistentComponent configPersistentComponent;
        if (!componentsMap.containsKey(activityId)) {
            configPersistentComponent = DaggerConfigPersistentComponent.builder()
                    .appComponent(App.get(this).getComponent())
                    .build();
            componentsMap.put(activityId, configPersistentComponent);
        } else {
            configPersistentComponent = componentsMap.get(activityId);
        }

        activityComponent = configPersistentComponent.activityComponent(new ActivityModule(this));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(KEY_ACTIVITY_ID, activityId);
    }

    @Override
    protected void onDestroy() {
        if (!isChangingConfigurations()) {
            componentsMap.remove(activityId);
        }
        super.onDestroy();
    }

    public ActivityComponent activityComponent() {
        return activityComponent;
    }
}
