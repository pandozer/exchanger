package com.usayplz.exchanger.ui.base;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public class BasePresenter<V extends MvpView> implements Presenter<V> {
    private V view;

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

    public V getView() {
        return view;
    }

    public boolean isViewAttached() {
        return view != null;
    }
}
