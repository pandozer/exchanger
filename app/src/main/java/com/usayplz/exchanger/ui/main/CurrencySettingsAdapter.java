package com.usayplz.exchanger.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.usayplz.exchanger.R;
import com.usayplz.exchanger.data.model.Currency;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kurikalov S. on 11.08.16
 */
public class CurrencySettingsAdapter extends RecyclerView.Adapter<CurrencySettingsAdapter.ViewHolder> {
    private List<Currency> currencies;
    private CurrencySettingsDialog.ICurrencySettingsListener listener;

    public CurrencySettingsAdapter(List<Currency> currencies, CurrencySettingsDialog.ICurrencySettingsListener listener) {
        this.currencies = currencies;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_currency_settings, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Currency currency = currencies.get(position);
        holder.flag.setImageResource(currency.getCurrencyFlagId());
        holder.currencyName.setText(String.format("%s (%s)", currency.getCurrencyName(), currency.getCurrencyCode()));

        if (currency.isBase()) {
            holder.baseAction.setImageResource(R.drawable.ic_main);
        } else {
            holder.baseAction.setImageResource(R.drawable.ic_main_off);
        }

        if (currency.isVisibility()) {
            holder.visibleAction.setImageResource(R.drawable.ic_visibility);
        } else {
            holder.visibleAction.setImageResource(R.drawable.ic_visibility_off);
        }

        holder.baseAction.setOnClickListener(view -> listener.setBase(currency));
        holder.visibleAction.setOnClickListener(view -> listener.setVisible(currency));
    }

    @Override
    public int getItemCount() {
        return currencies != null ? currencies.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.flag) ImageView flag;
        @BindView(R.id.currency_name) TextView currencyName;
        @BindView(R.id.base_action) ImageView baseAction;
        @BindView(R.id.visible_action) ImageView visibleAction;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
        notifyDataSetChanged();
    }
}