package com.usayplz.exchanger.ui.main;

import com.usayplz.exchanger.data.model.Currency;
import com.usayplz.exchanger.ui.base.MvpView;

import java.util.Date;
import java.util.List;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public interface MainView extends MvpView {
    void showCurrencies(List<Currency> currencies, List<Currency> showCurrencies);

    void showError(int message);

    void startUpdate();

    void stopUpdate(Date last, boolean synced);

    void openSettingsDialog(List<Currency> currencies);

    void initUI(String basePrice, boolean mode);

    void setMode(boolean mode);
}
