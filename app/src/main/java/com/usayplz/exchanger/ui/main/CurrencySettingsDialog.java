package com.usayplz.exchanger.ui.main;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.usayplz.exchanger.R;
import com.usayplz.exchanger.data.model.Currency;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kurikalov S. on 10.08.16
 */
public class CurrencySettingsDialog extends DialogFragment {

    private static final String TAG = "currency_settings";
    private ICurrencySettingsListener listener;
    private CurrencySettingsAdapter adapter;
    private List<Currency> currencies;

    @BindView(R.id.list) RecyclerView list;
    @BindView(R.id.filter) EditText filter;
    private String query;


    public CurrencySettingsDialog() {
    }

    public void show(FragmentManager manager, List<Currency> currencies, ICurrencySettingsListener listener) {
        super.show(manager, TAG);
        this.currencies = currencies;
        this.listener = listener;
        this.query = "";
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_currency_settings, null);

        ButterKnife.bind(this, view);

        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setHasFixedSize(true);
        list.setItemAnimator(new DefaultItemAnimator());

        adapter = new CurrencySettingsAdapter(currencies, listener);
        list.setAdapter(adapter);

        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                query = editable.toString().toLowerCase();
                adapter.setCurrencies(filterCurrencies());
            }
        });
        return new AlertDialog.Builder(getActivity())
                .setTitle(null)
                .setView(view)
                .setNegativeButton(R.string.dialog_close, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .create();
    }

    private List<Currency> filterCurrencies() {
        if (this.query.length() > 0) {
            List<Currency> result = new ArrayList<>();
            String search = "";
            for (Currency currency : currencies) {
                search = String.format("%s %s", currency.getCurrencyName(), currency.getCurrencyCode()).toLowerCase();
                if (search.contains(query)) {
                    result.add(currency);
                }
            }
            return result;
        }
        return this.currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
        adapter.setCurrencies(filterCurrencies());
    }

    public interface ICurrencySettingsListener {
        void setBase(Currency currency);

        void setVisible(Currency currency);
    }
}
