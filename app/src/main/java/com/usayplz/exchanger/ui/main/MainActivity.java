package com.usayplz.exchanger.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.usayplz.exchanger.R;
import com.usayplz.exchanger.data.model.Currency;
import com.usayplz.exchanger.ui.base.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView {
    @Inject MainPresenter presenter;

    @BindView(R.id.list) RecyclerView list;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.flag) ImageView baseFlag;
    @BindView(R.id.currency_name) TextView baseName;
    @BindView(R.id.base_price) EditText basePrice;
    @BindView(R.id.currency_layout) LinearLayout currencyLayout;
    @BindView(R.id.dropdown) ImageView dropdown;
    @BindView(R.id.base_caption) TextView baseCaption;
    @BindView(R.id.fab) FloatingActionButton fab;

    private SwitchCompat modeSwitcher;
    private CurrencyAdapter adapter;
    private CurrencySettingsDialog currencySettingsDialog;
    private boolean presenterAttached = false;

    private TextWatcher PriceWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            presenter.priceChanged(String.valueOf(charSequence));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        modeSwitcher = (SwitchCompat) menu.findItem(R.id.action_mode).getActionView().findViewById(R.id.mode);

        initPresenter();

        return true;
    }

    private void initPresenter() {
        // check all views is ready and presenter not atached
        if (!presenterAttached && modeSwitcher != null && currencyLayout != null) {
            presenterAttached = true;
            activityComponent().inject(this);
            presenter.attachView(this);
            presenter.init();
        }
    }

    // Implements MainView //
    @Override
    public void initUI(String price, boolean mode) {
        currencySettingsDialog = new CurrencySettingsDialog();

        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(true);
        list.setItemAnimator(new DefaultItemAnimator());

        adapter = new CurrencyAdapter(currency -> presenter.openSettingsDialog(), mode);
        list.setAdapter(adapter);

        basePrice.setText(price);
        basePrice.setSelection(basePrice.length());
        basePrice.addTextChangedListener(PriceWatcher);

        currencyLayout.setOnClickListener(view -> presenter.openSettingsDialog());
        dropdown.setOnClickListener(view -> presenter.openSettingsDialog());

        modeSwitcher.setOnCheckedChangeListener((compoundButton, state) -> presenter.setMode(state));

        fab.setOnClickListener(view -> presenter.syncCurrencies());

        list.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int heightDiff = list.getRootView().getHeight() - list.getHeight();
            if (heightDiff > MetricUtils.dpToPx(this, 200)) {
                fab.setVisibility(View.INVISIBLE);
            } else {
                fab.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void setMode(boolean mode) {
        modeSwitcher.setChecked(mode);

        if (adapter != null) {
            adapter.changeMode(mode);
        }

        if (mode) { // calc
            toolbar.setTitle(R.string.mode_calculator);
            basePrice.setVisibility(View.VISIBLE);

            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            basePrice.requestFocus();
            basePrice.setSelection(basePrice.length());
            inputMethodManager.showSoftInput(basePrice, 0);
        } else { // rate
            toolbar.setTitle(R.string.mode_rates);
            basePrice.clearFocus();
            basePrice.setVisibility(View.INVISIBLE);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(basePrice.getWindowToken(), 0);
        }
    }

    @Override
    public void showCurrencies(List<Currency> currencies, List<Currency> showCurrencies) {
        Currency baseCurrency = Currency.getBase(currencies);

        baseFlag.setImageResource(baseCurrency.getCurrencyFlagId());
        baseName.setText(String.format("%s (%s)", baseCurrency.getCurrencyName(), baseCurrency.getCurrencyCode()));
        adapter.setCurrencies(showCurrencies);

        if (currencySettingsDialog.isAdded()) {
            currencySettingsDialog.setCurrencies(currencies);
        }
    }

    @Override
    public void openSettingsDialog(List<Currency> currencies) {
        if (currencySettingsDialog.isAdded()) {
            currencySettingsDialog.dismiss();
        }
        currencySettingsDialog.show(getSupportFragmentManager(), currencies, new CurrencySettingsDialog.ICurrencySettingsListener() {
            @Override
            public void setBase(Currency currency) {
                presenter.setBase(currency);
            }

            @Override
            public void setVisible(Currency currency) {
                presenter.setVisible(currency);
            }
        });
    }

    @Override
    public void showError(int message) {
        Snackbar.make(currencyLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void startUpdate() {
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(500);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());

        fab.setEnabled(false);
        fab.startAnimation(rotate);
    }

    @Override
    public void stopUpdate(Date last, boolean synced) {
        if (synced) {
            Snackbar.make(currencyLayout, R.string.updated, Snackbar.LENGTH_LONG).show();
        }

        if (!fab.isEnabled()) {
            fab.setEnabled(true);
            fab.clearAnimation();
        }

        if (last.getTime() > 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
            baseCaption.setText(String.format("%s (%s: %s %s)", getString(R.string.base_caption),
                    getString(R.string.base_updated),
                    dateFormat.format(last),
                    DateFormat.getTimeFormat(this).format(last)));
        }
    }
}
