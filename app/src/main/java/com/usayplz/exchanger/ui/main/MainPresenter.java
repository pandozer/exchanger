package com.usayplz.exchanger.ui.main;

import android.content.Context;

import com.usayplz.exchanger.R;
import com.usayplz.exchanger.data.DataManager;
import com.usayplz.exchanger.data.local.DbOpenHelper;
import com.usayplz.exchanger.data.model.Currency;
import com.usayplz.exchanger.data.model.CurrencyYahoo;
import com.usayplz.exchanger.injection.scope.AppContext;
import com.usayplz.exchanger.injection.scope.ConfigPersistent;
import com.usayplz.exchanger.ui.base.BasePresenter;
import com.usayplz.exchanger.util.FileUtils;
import com.usayplz.exchanger.util.Log;
import com.usayplz.exchanger.util.Strings;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@ConfigPersistent
public class MainPresenter extends BasePresenter<MainView> {

    private static final String COUNTRY_FLAG_US = "us";
    private static final String COUNTRY_FLAG_EU = "eu";
    public static final String BASE_CURRENCY_RATE = "BTC";
    private static final BigDecimal MIN_SHOWING_PRICE = new BigDecimal(0.01);
    private final DataManager dataManager;
    private final Context context;
    private Disposable disposable;
    private List<Currency> currencies = new ArrayList<>();
    private String basePrice;
    private boolean mode;
    private boolean syncing;

    @Inject
    public MainPresenter(DataManager dataManager, @AppContext Context context) {
        this.dataManager = dataManager;
        this.context = context;
        this.syncing = false;
    }

    @Override
    public void detachView() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

        super.detachView();
    }

    public void init() {
        if (dataManager.getPreferencesHelper().getFirstLaunch()) {
            firstLaunch();
        }

        this.mode = dataManager.getPreferencesHelper().getMode();
        Date last = dataManager.getPreferencesHelper().getLastUpdate();
        this.basePrice = dataManager.getPreferencesHelper().getBasePrice();

        if (isViewAttached()) {
            getView().stopUpdate(last, false);
            getView().setMode(mode);
            getView().initUI(basePrice, mode);
        }

        loadCurrencies();
    }


    private void firstLaunch() {
        try {
            File dbFile = FileUtils.loadAsset(context, DbOpenHelper.DATABASE_NAME);
            dataManager.importTable(dbFile.getAbsolutePath());
        } catch (Exception e) {
            Log.d(e.getMessage());
        }


        String countryCode = Locale.getDefault().getCountry().toLowerCase();
        int baseCurrencyPosition = -1;

        List<Currency> currencies = dataManager.getAllCurrencies();

        for (Currency currency : currencies) {
            String flag = currency.getCurrencyFlag();
            int resId = context.getResources().getIdentifier(flag, "drawable", context.getPackageName());
            currency.setCurrencyFlagId(resId);

            if (countryCode.equals(flag)) {
                currency.setVisibility(true);
                baseCurrencyPosition = currencies.indexOf(currency);
            }

            if (flag.equals(COUNTRY_FLAG_US) || flag.equals(COUNTRY_FLAG_EU)) {
                currency.setVisibility(true);
            }

            if (flag.equals(COUNTRY_FLAG_US) && baseCurrencyPosition == -1) {
                baseCurrencyPosition = currencies.indexOf(currency);
            }
        }

        currencies.get(baseCurrencyPosition).setBase(true);
        dataManager.getPreferencesHelper().setFirstLaunch(false);
        dataManager.setCurrencies(currencies);
    }

    public void loadCurrencies() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = dataManager.getCurrencies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(currencies -> {
                            MainPresenter.this.currencies = currencies;
                            Date nextUpdate = new Date(dataManager.getPreferencesHelper().getLastUpdate().getTime() + TimeUnit.MINUTES.toMillis(30));
                            if (!syncing && new Date().after(nextUpdate)) {
                                syncCurrencies();
                            }

                            if (isViewAttached()) {
                                getView().showCurrencies(currencies, modeFilteredCurrencies());
                            }
                        },
                        throwable -> {
                            Log.d("Error get currencies: " + throwable.getMessage());
                            if (isViewAttached()) {
                                getView().showError(R.string.error_get_currencies);
                            }
                        });
    }

    private List<Currency> modeFilteredCurrencies() {
        BigDecimal quality = BigDecimal.ZERO.setScale(10, BigDecimal.ROUND_HALF_UP);
        if (!Strings.isEmpty(basePrice)) {
            quality = Strings.toBigDecimal(basePrice).setScale(10, BigDecimal.ROUND_HALF_UP);
        }

        Currency baseCurrency = Currency.getBase(currencies);
        int normalization;

        List<Currency> result = new ArrayList<>();
        for (Currency currency : currencies) {
            if (baseCurrency != null && currency.isVisibility() && !currency.isBase()) {
                BigDecimal price = currency.getPrice();
                BigDecimal newPrice = BigDecimal.ZERO.setScale(10, BigDecimal.ROUND_HALF_UP);

                if (baseCurrency.getPrice() != null && price != null && price.compareTo(BigDecimal.ZERO) != 0) {
                    newPrice = baseCurrency.getPrice().setScale(10, BigDecimal.ROUND_HALF_UP).divide(price, BigDecimal.ROUND_HALF_UP);
                }

                if (mode) {
                    if (quality != null && newPrice.compareTo(BigDecimal.ZERO) != 0) {
                        newPrice = quality.divide(newPrice, BigDecimal.ROUND_HALF_UP);
                    } else {
                        newPrice = BigDecimal.ZERO;
                    }
                } else {
                    normalization = 1;
                    if (newPrice.compareTo(BigDecimal.ZERO) > 0 && newPrice.compareTo(MIN_SHOWING_PRICE) < 0) { // newPrice < 0.01
                        normalization = (int) Math.pow(10, Strings.zeroCount(newPrice.toPlainString()) - 2);
                        newPrice = newPrice.multiply(new BigDecimal(normalization));
                    }
                    currency.setNormalization(normalization);
                }

                currency.setBasePrice(newPrice.setScale(4, BigDecimal.ROUND_HALF_UP));
                result.add(currency);
            }
        }

        return result;
    }

    public void setMode(boolean mode) {
        this.mode = mode;
        dataManager.getPreferencesHelper().setMode(mode);

        if (isViewAttached()) {
            getView().setMode(mode);
            getView().showCurrencies(currencies, modeFilteredCurrencies());
        }
    }

    public void setBase(Currency currency) {
        if (!currency.isBase()) {
            currency.setBase(true);
            dataManager.setBase(currency);
        }
    }

    public void setVisible(Currency currency) {
        dataManager.setVisible(currency);
    }

    public void syncCurrencies() {
        syncing = true;

        if (isViewAttached()) {
            getView().startUpdate();
        }

        dataManager.syncCurrencies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .subscribe(currencyYahoo -> {
                            List<CurrencyYahoo.Rate> rates = currencyYahoo.getQuery().getResults().getRate();
                            dataManager.setRates(rates);
                            dataManager.getPreferencesHelper().setLastUpdate();
                            syncing = false;

                            if (isViewAttached()) {
                                getView().stopUpdate(new Date(), true);
                            }
                        },
                        throwable -> {
                            Log.d("Error syncing: " + throwable.getMessage());
                            if (isViewAttached()) {
                                getView().showError(R.string.error_update_currencies);
                                getView().stopUpdate(dataManager.getPreferencesHelper().getLastUpdate(), false);
                                syncing = false;
                            }
                        });
    }

    public void openSettingsDialog() {
        if (isViewAttached()) {
            getView().openSettingsDialog(this.currencies);
        }
    }

    public void priceChanged(String textPrice) {
        if (!Strings.isEmpty(textPrice)) {
            this.basePrice = textPrice;
            dataManager.getPreferencesHelper().setBasePrice(textPrice);
        }

        if (isViewAttached()) {
            getView().showCurrencies(currencies, modeFilteredCurrencies());
        }
    }
}
