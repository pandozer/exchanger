package com.usayplz.exchanger.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.usayplz.exchanger.R;
import com.usayplz.exchanger.data.model.Currency;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kurikalov S. on 07.08.16
 */
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {
    private final ICurrencyAdapterListener listener;
    private List<Currency> currencies;
    private boolean mode;

    public CurrencyAdapter(ICurrencyAdapterListener listener, boolean mode) {
        this.currencies = new ArrayList<>();
        this.listener = listener;
        this.mode = mode;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_currency, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Currency currency = currencies.get(position);
        holder.flag.setImageResource(currency.getCurrencyFlagId());
        holder.price.setText(currency.getBasePrice().toPlainString());
        holder.layout.setOnClickListener(view -> listener.itemClick(currency));

        if (mode) {
            holder.currencyName.setText(String.format("%s (%s)", currency.getCurrencyName(), currency.getCurrencyCode()));
        } else {
            holder.currencyName.setText(String.format("%s %s (%s)", currency.getNormalization(), currency.getCurrencyName(), currency.getCurrencyCode()));
        }
    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }

    public void changeMode(boolean mode) {
        this.mode = mode;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.flag) ImageView flag;
        @BindView(R.id.currency_name) TextView currencyName;
        @BindView(R.id.price) TextView price;
        @BindView(R.id.item_layout) RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
        notifyDataSetChanged();
    }

    public interface ICurrencyAdapterListener {
        void itemClick(Currency currency);
    }
}
