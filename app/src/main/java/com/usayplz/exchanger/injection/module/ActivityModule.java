package com.usayplz.exchanger.injection.module;

import android.app.Activity;
import android.content.Context;

import com.usayplz.exchanger.injection.scope.ActivityContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Module
public class ActivityModule {
    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return activity;
    }
}
