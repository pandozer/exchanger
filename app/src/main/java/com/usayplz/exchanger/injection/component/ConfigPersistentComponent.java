package com.usayplz.exchanger.injection.component;

import com.usayplz.exchanger.injection.module.ActivityModule;
import com.usayplz.exchanger.injection.scope.ConfigPersistent;

import dagger.Component;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@ConfigPersistent
@Component(dependencies = AppComponent.class)
public interface ConfigPersistentComponent {
    ActivityComponent activityComponent(ActivityModule activityModule);
}
