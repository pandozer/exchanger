package com.usayplz.exchanger.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
