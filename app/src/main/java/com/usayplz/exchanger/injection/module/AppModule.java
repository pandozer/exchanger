package com.usayplz.exchanger.injection.module;

import android.app.Application;
import android.content.Context;

import com.usayplz.exchanger.data.remote.CurrencyYahooService;
import com.usayplz.exchanger.injection.scope.AppContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Module
public class AppModule {
    protected final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @AppContext
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    CurrencyYahooService provideCurrencyYahooService() {
        return CurrencyYahooService.Creator.newCurrencyYahooService();
    }
}
