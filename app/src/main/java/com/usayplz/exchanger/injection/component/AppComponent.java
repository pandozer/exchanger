package com.usayplz.exchanger.injection.component;

import android.app.Application;
import android.content.Context;

import com.usayplz.exchanger.data.DataManager;
import com.usayplz.exchanger.data.local.DatabaseHelper;
import com.usayplz.exchanger.data.local.PreferencesHelper;
import com.usayplz.exchanger.data.remote.CurrencyYahooService;
import com.usayplz.exchanger.injection.module.AppModule;
import com.usayplz.exchanger.injection.scope.AppContext;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Kurikalov S. on 05.08.16
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    @AppContext
    Context context();

    Application application();

    CurrencyYahooService currencyYahooService();

    PreferencesHelper preferencesHelper();

    DatabaseHelper databaseHelper();

    DataManager dataManager();
}
