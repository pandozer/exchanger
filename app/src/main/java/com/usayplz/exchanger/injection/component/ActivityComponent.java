package com.usayplz.exchanger.injection.component;

import com.usayplz.exchanger.injection.module.ActivityModule;
import com.usayplz.exchanger.injection.scope.PerActivity;
import com.usayplz.exchanger.ui.main.MainActivity;

import dagger.Subcomponent;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
}
