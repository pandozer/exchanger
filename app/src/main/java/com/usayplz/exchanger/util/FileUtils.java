package com.usayplz.exchanger.util;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Kurikalov S. on 08.08.16
 */
public class FileUtils {
    public static File concatToFile(String... args) {
        return new File(TextUtils.join(File.separator, args));
    }

    public static File loadAsset(Context context, String fileName) throws IOException {
        File target = concatToFile(context.getCacheDir().getAbsolutePath(), fileName);
        InputStream inputStream = context.getAssets().open(fileName);
        OutputStream outputStream = new FileOutputStream(target);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();

        return target;
    }
}
