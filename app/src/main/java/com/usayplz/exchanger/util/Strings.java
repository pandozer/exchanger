package com.usayplz.exchanger.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Locale;

/**
 * Created by Kurikalov S. on 10.08.16
 */
public class Strings {

    public static BigDecimal toBigDecimalUs(String s) {
        DecimalFormat numberFormat = (DecimalFormat) NumberFormat.getInstance(new Locale("en", "US"));
        numberFormat.setParseBigDecimal(true);

        try {
            return (BigDecimal) numberFormat.parse(s, new ParsePosition(0));
        } catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }

    public static BigDecimal toBigDecimal(String s) {
        try {
            return new BigDecimal(s);
        } catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }

    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static int zeroCount(String s) {
        int count = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '.' || s.charAt(i) == ',') {
                continue;
            }

            if (s.charAt(i) == '0') {
                count++;
            } else {
                break;
            }
        }

        return count - 1; // minus first zero
    }
}
