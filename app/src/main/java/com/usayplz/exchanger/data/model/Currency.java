package com.usayplz.exchanger.data.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public class Currency {
    private long id;
    private String countryName;
    private String countryFlag;
    private String currencyName;
    private String currencyCode;
    private String currencyFlag;
    private BigDecimal price;
    private boolean visibility;
    private int currencyFlagId;
    private boolean base;
    private BigDecimal basePrice;
    private int normalization;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyFlag() {
        return currencyFlag;
    }

    public void setCurrencyFlag(String currencyFlag) {
        this.currencyFlag = currencyFlag;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public String getCountryFlag() {
        return countryFlag;
    }

    public void setCountryFlag(String countryFlag) {
        this.countryFlag = countryFlag;
    }

    public int getCurrencyFlagId() {
        return currencyFlagId;
    }

    public void setCurrencyFlagId(int currencyFlagId) {
        this.currencyFlagId = currencyFlagId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public boolean isBase() {
        return base;
    }

    public void setBase(boolean base) {
        this.base = base;
    }

    public static Currency getBase(List<Currency> currencies) {
        for (Currency currency : currencies) {
            if (currency.isBase()) {
                return currency;
            }
        }
        return null;
    }

    public int getNormalization() {
        return normalization;
    }

    public void setNormalization(int normalization) {
        this.normalization = normalization;
    }
}
