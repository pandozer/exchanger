package com.usayplz.exchanger.data.remote;

import com.usayplz.exchanger.data.model.CurrencyYahoo;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Kurikalov S. on 10.08.16
 */
public interface CurrencyYahooService {
    String ENDPOINT = "https://query.yahooapis.com/v1/public/";
    String QUERY = "select * from yahoo.finance.xchange where pair in (\"BTCBTC\",\"BTCAED\",\"BTCAFN\",\"BTCALL\",\"BTCAMD\",\"BTCAOA\",\"BTCARS\",\"BTCAUD\",\"BTCAWG\",\"BTCAZN\",\"BTCBAM\",\"BTCBBD\",\"BTCBDT\",\"BTCBGN\",\"BTCBHD\",\"BTCBIF\",\"BTCBMD\",\"BTCBND\",\"BTCBOB\",\"BTCBRL\",\"BTCBSD\",\"BTCBWP\",\"BTCBYR\",\"BTCBZD\",\"BTCCAD\",\"BTCCDF\",\"BTCCHF\",\"BTCCLP\",\"BTCCNY\",\"BTCCOP\",\"BTCCRC\",\"BTCCUP\",\"BTCCVE\",\"BTCCZK\",\"BTCDJF\",\"BTCDKK\",\"BTCDOP\",\"BTCDZD\",\"BTCEGP\",\"BTCERN\",\"BTCETB\",\"BTCEUR\",\"BTCFJD\",\"BTCFKP\",\"BTCGBP\",\"BTCGEL\",\"BTCGHS\",\"BTCGIP\",\"BTCGMD\",\"BTCGNF\",\"BTCGTQ\",\"BTCGYD\",\"BTCHKD\",\"BTCHNL\",\"BTCHRK\",\"BTCHUF\",\"BTCIDR\",\"BTCILS\",\"BTCINR\",\"BTCIQD\",\"BTCIRR\",\"BTCISK\",\"BTCJMD\",\"BTCJOD\",\"BTCJPY\",\"BTCKES\",\"BTCKGS\",\"BTCKHR\",\"BTCKMF\",\"BTCKPW\",\"BTCKRW\",\"BTCKWD\",\"BTCKYD\",\"BTCKZT\",\"BTCLAK\",\"BTCLBP\",\"BTCLKR\",\"BTCLRD\",\"BTCLYD\",\"BTCMAD\",\"BTCMDL\",\"BTCMGA\",\"BTCMKD\",\"BTCMMK\",\"BTCMNT\",\"BTCMOP\",\"BTCMRO\",\"BTCMUR\",\"BTCMVR\",\"BTCMWK\",\"BTCMXN\",\"BTCMYR\",\"BTCMZN\",\"BTCNGN\",\"BTCNIO\",\"BTCNOK\",\"BTCNPR\",\"BTCNZD\",\"BTCOMR\",\"BTCPEN\",\"BTCPGK\",\"BTCPHP\",\"BTCPKR\",\"BTCPLN\",\"BTCPYG\",\"BTCQAR\",\"BTCRON\",\"BTCRUB\",\"BTCRWF\",\"BTCSAR\",\"BTCSBD\",\"BTCSCR\",\"BTCSDG\",\"BTCSEK\",\"BTCSGD\",\"BTCSHP\",\"BTCSLL\",\"BTCSOS\",\"BTCSRD\",\"BTCSTD\",\"BTCSYP\",\"BTCSZL\",\"BTCTHB\",\"BTCTJS\",\"BTCTMT\",\"BTCTND\",\"BTCTOP\",\"BTCTRY\",\"BTCTTD\",\"BTCTZS\",\"BTCUAH\",\"BTCUGX\",\"BTCUSD\",\"BTCUYU\",\"BTCUZS\",\"BTCVEF\",\"BTCVND\",\"BTCVUV\",\"BTCWST\",\"BTCXAF\",\"BTCXCD\",\"BTCXOF\",\"BTCXPF\",\"BTCYER\",\"BTCZAR\",\"BTCZMW\",\"BTCZWL\")";
    String FORMAT = "json";
    String ENV = "store://datatables.org/alltableswithkeys";

    @GET("yql?")
    Observable<CurrencyYahoo> getCurrencies(@Query("q") String data, @Query("format") String format, @Query("env") String env);

    class Creator {
        public static CurrencyYahooService newCurrencyYahooService() {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(CurrencyYahooService.class);
        }
    }
}