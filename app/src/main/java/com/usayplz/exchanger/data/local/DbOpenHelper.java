package com.usayplz.exchanger.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.usayplz.exchanger.injection.scope.AppContext;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Singleton
public class DbOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "exchanger.db";
    public static final int DATABASE_VERSION = 1;

    @Inject
    public DbOpenHelper(@AppContext Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onConfigure(SQLiteDatabase sqLiteDatabase) {
        super.onConfigure(sqLiteDatabase);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        importTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) { }

    private void importTable(SQLiteDatabase db) { }
}
