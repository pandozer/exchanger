package com.usayplz.exchanger.data.local;

import android.database.Cursor;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;
import com.usayplz.exchanger.data.model.Currency;
import com.usayplz.exchanger.data.model.CurrencyYahoo;
import com.usayplz.exchanger.ui.main.MainPresenter;
import com.usayplz.exchanger.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import hu.akarnokd.rxjava.interop.RxJavaInterop;
import io.reactivex.Flowable;
import rx.schedulers.Schedulers;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Singleton
public class DatabaseHelper {
    private final BriteDatabase db;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        db = sqlBrite.wrapDatabaseHelper(dbOpenHelper, Schedulers.io());
    }

    public BriteDatabase getDb() {
        return db;
    }

    public Flowable<List<Currency>> getCurrencies() {
        return RxJavaInterop.toV2Flowable(db.createQuery(Db.CurrencyTable.TABLE,
                String.format("SELECT * FROM %s group by %s order by %s desc, %s",
                        Db.CurrencyTable.TABLE,
                        Db.CurrencyTable.COLUMN_CURRENCY_CODE,
                        Db.CurrencyTable.COLUMN_VISIBILITY,
                        Db.CurrencyTable.COLUMN_CURRENCY_NAME))
                .mapToList(Db.CurrencyTable.MAPPER)
                .subscribeOn(Schedulers.io()));
    }

    public void importTable(String dbFile) {
        try {
            db.execute("DROP TABLE IF EXISTS " + Db.CurrencyTable.TABLE + ";");
            db.execute("ATTACH DATABASE '" + dbFile + "' AS NEW_DB;");
            db.execute("CREATE TABLE " + Db.CurrencyTable.TABLE + " AS SELECT * FROM NEW_DB." + Db.CurrencyTable.TABLE + ";");
            db.execute("DETACH NEW_DB;");
        } catch (Exception e) {
            Log.d(e.getMessage());
        }
    }

    public void update(Currency currency) {
        db.update(Db.CurrencyTable.TABLE, Db.CurrencyTable.toContentValues(currency),
                Db.CurrencyTable.COLUMN_CURRENCY_CODE + "=?", String.valueOf(currency.getCurrencyCode()));
    }

    public void setRates(List<CurrencyYahoo.Rate> rates) {
        BriteDatabase.Transaction transaction = db.newTransaction();
        try {
            for (CurrencyYahoo.Rate rate : rates) {
                db.executeAndTrigger(Db.CurrencyTable.TABLE, String.format("update %s set %s = '%s' where %s = '%s'",
                        Db.CurrencyTable.TABLE,
                        Db.CurrencyTable.COLUMN_PRICE,
                        rate.getRate(),
                        Db.CurrencyTable.COLUMN_CURRENCY_CODE,
                        rate.getId().replaceFirst(MainPresenter.BASE_CURRENCY_RATE, "")));
            }
            transaction.markSuccessful();
        } finally {
            transaction.end();
        }
    }

    public void setCurrencies(List<Currency> currencies) {
        BriteDatabase.Transaction transaction = db.newTransaction();
        try {
            for (Currency currency : currencies) {
                update(currency);
            }
            transaction.markSuccessful();
        } finally {
            transaction.end();
        }
    }

    public void setBase(Currency currency) {
        BriteDatabase.Transaction transaction = db.newTransaction();
        try {
            db.execute(String.format("update %s set %s = 0",
                    Db.CurrencyTable.TABLE,
                    Db.CurrencyTable.COLUMN_BASE));
            currency.setVisibility(true);
            update(currency);
            transaction.markSuccessful();
        } finally {
            transaction.end();
        }
    }

    public void setVisible(Currency currency) {
        currency.setVisibility(!currency.isVisibility());
        update(currency);
    }

    public List<Currency> getAllCurrencies() {
        List<Currency> currencies = new ArrayList<>();
        Cursor cursor = db.query("SELECT * FROM " + Db.CurrencyTable.TABLE);
        while (cursor.moveToNext()) {
            currencies.add(Db.CurrencyTable.MAPPER.call(cursor));
        }
        return currencies;
    }
}
