package com.usayplz.exchanger.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Kurikalov S. on 10.08.16
 */
public class CurrencyYahoo implements Serializable {
    private Query query;

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public class Query {
        private int count;
        private String created;
        private String lang;
        private Results results;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public Results getResults() {
            return results;
        }

        public void setResults(Results results) {
            this.results = results;
        }
    }

    public class Results {
        private List<Rate> rate;

        public List<Rate> getRate() {
            return rate;
        }

        public void setRate(List<Rate> rate) {
            this.rate = rate;
        }
    }

    public class Rate {
        private String id;
        @SerializedName("Name")
        private String name;
        @SerializedName("Rate")
        private String rate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }
    }
}
