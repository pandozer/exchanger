package com.usayplz.exchanger.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import com.usayplz.exchanger.data.model.Currency;
import com.usayplz.exchanger.util.Strings;

import rx.functions.Func1;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public class Db {
    public final static int BOOLEAN_FALSE = 0;
    public final static int BOOLEAN_TRUE = 1;

    public Db() {
    }

    public static String getString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }

    public static boolean getBoolean(Cursor cursor, String columnName) {
        return getInt(cursor, columnName) == BOOLEAN_TRUE;
    }

    public static long getLong(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(columnName));
    }

    public static int getInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }

    public static class CurrencyTable {
        public static final String TABLE = "currency";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_COUNTRY_NAME = "country_name";
        public static final String COLUMN_COUNTRY_FLAG = "country_flag";
        public static final String COLUMN_CURRENCY_NAME = "currency_name";
        public static final String COLUMN_CURRENCY_CODE = "currency_code";
        public static final String COLUMN_CURRENCY_FLAG = "currency_flag";
        public static final String COLUMN_CURRENCY_FLAG_ID = "currency_flag_id";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_BASE = "base";
        public static final String COLUMN_VISIBILITY = "visibility";

        public static final String CREATE =
                "CREATE TABLE " + TABLE + " (" +
                        COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        COLUMN_COUNTRY_NAME + " TEXT, " +
                        COLUMN_COUNTRY_FLAG + " TEXT, " +
                        COLUMN_CURRENCY_NAME + " TEXT, " +
                        COLUMN_CURRENCY_CODE + " TEXT, " +
                        COLUMN_CURRENCY_FLAG + " TEXT, " +
                        COLUMN_CURRENCY_FLAG_ID + " INTEGER, " +
                        COLUMN_PRICE + " TEXT, " +
                        COLUMN_BASE + " INTEGER, " +
                        COLUMN_VISIBILITY + " INTEGER " +
                        " ); ";

        public static ContentValues toContentValues(Currency currency) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, currency.getId());
            values.put(COLUMN_COUNTRY_NAME, currency.getCountryName());
            values.put(COLUMN_COUNTRY_FLAG, currency.getCountryFlag());
            values.put(COLUMN_CURRENCY_NAME, currency.getCurrencyName());
            values.put(COLUMN_CURRENCY_CODE, currency.getCurrencyCode());
            values.put(COLUMN_CURRENCY_FLAG, currency.getCurrencyFlag());
            values.put(COLUMN_CURRENCY_FLAG_ID, currency.getCurrencyFlagId());
            values.put(COLUMN_PRICE, currency.getPrice().toPlainString());
            values.put(COLUMN_BASE, currency.isBase() ? BOOLEAN_TRUE : BOOLEAN_FALSE);
            values.put(COLUMN_VISIBILITY, currency.isVisibility() ? BOOLEAN_TRUE : BOOLEAN_FALSE);
            return values;
        }

        public static final Func1<Cursor, Currency> MAPPER = cursor -> {
            Currency currency = new Currency();
            currency.setId(getLong(cursor, COLUMN_ID));
            currency.setCountryName(getString(cursor, COLUMN_COUNTRY_NAME));
            currency.setCountryFlag(getString(cursor, COLUMN_COUNTRY_FLAG));
            currency.setCurrencyName(getString(cursor, COLUMN_CURRENCY_NAME));
            currency.setCurrencyCode(getString(cursor, COLUMN_CURRENCY_CODE));
            currency.setCurrencyFlag(getString(cursor, COLUMN_CURRENCY_FLAG));
            currency.setCurrencyFlagId(getInt(cursor, COLUMN_CURRENCY_FLAG_ID));
            currency.setPrice(Strings.toBigDecimalUs(getString(cursor, COLUMN_PRICE)));
            currency.setBase(getBoolean(cursor, COLUMN_BASE));
            currency.setVisibility(getBoolean(cursor, COLUMN_VISIBILITY));
            return currency;
        };
    }
}
