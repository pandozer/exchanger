package com.usayplz.exchanger.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.usayplz.exchanger.injection.scope.AppContext;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Singleton
public class PreferencesHelper {
    public static final String PREF_FILE_NAME = "pref_file";
    public static final String PREF_FIRST_LAUNCH = "pref_first_launch";
    public static final String PREF_LAST_UPDATE = "pref_last_update";
    private static final String PREF_MODE = "pref_mode";
    private static final String PREF_BASE_PRICE = "pref_base_price";

    private final SharedPreferences pref;

    @Inject
    public PreferencesHelper(@AppContext Context context) {
        pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        pref.edit().clear().apply();
    }

    public boolean getFirstLaunch() {
        return pref.getBoolean(PREF_FIRST_LAUNCH, true);
    }

    public void setFirstLaunch(boolean launch) {
        pref.edit().putBoolean(PREF_FIRST_LAUNCH, launch).apply();
    }

    public Date getLastUpdate() {
        return new Date(pref.getLong(PREF_LAST_UPDATE, 0));
    }

    public void setLastUpdate() {
        pref.edit().putLong(PREF_LAST_UPDATE, new Date().getTime()).apply();
    }

    public void setMode(boolean state) {
        pref.edit().putBoolean(PREF_MODE, state).apply();
    }

    public boolean getMode() {
        return pref.getBoolean(PREF_MODE, false);
    }

    public String getBasePrice() {
        return pref.getString(PREF_BASE_PRICE, "1");
    }

    public void setBasePrice(String price) {
        pref.edit().putString(PREF_BASE_PRICE, price).apply();
    }
}
