package com.usayplz.exchanger.data;

import com.usayplz.exchanger.data.local.DatabaseHelper;
import com.usayplz.exchanger.data.local.PreferencesHelper;
import com.usayplz.exchanger.data.model.Currency;
import com.usayplz.exchanger.data.model.CurrencyYahoo;
import com.usayplz.exchanger.data.remote.CurrencyYahooService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import hu.akarnokd.rxjava.interop.RxJavaInterop;
import io.reactivex.Flowable;

/**
 * Created by Kurikalov S. on 05.08.16
 */
@Singleton
public class DataManager {
    private final DatabaseHelper databaseHelper;
    private final PreferencesHelper preferencesHelper;
    private final CurrencyYahooService currencyYahooService;

    @Inject
    public DataManager(DatabaseHelper databaseHelper,
                       PreferencesHelper preferencesHelper,
                       CurrencyYahooService currencyYahooService) {
        this.databaseHelper = databaseHelper;
        this.preferencesHelper = preferencesHelper;
        this.currencyYahooService = currencyYahooService;
    }

    public DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }

    public Flowable<List<Currency>> getCurrencies() {
        return databaseHelper.getCurrencies();
    }

    public void importTable(String dbFile) {
        databaseHelper.importTable(dbFile);
    }

    public Flowable<CurrencyYahoo> syncCurrencies() {
        return RxJavaInterop.toV2Flowable(currencyYahooService.getCurrencies(CurrencyYahooService.QUERY, CurrencyYahooService.FORMAT, CurrencyYahooService.ENV));
    }

    public void setRates(List<CurrencyYahoo.Rate> rates) {
        databaseHelper.setRates(rates);
    }

    public void setCurrencies(List<Currency> currencies) {
        databaseHelper.setCurrencies(currencies);
    }

    public void setBase(Currency currency) {
        databaseHelper.setBase(currency);
    }

    public void setVisible(Currency currency) {
        databaseHelper.setVisible(currency);
    }

    public List<Currency> getAllCurrencies() {
        return databaseHelper.getAllCurrencies();
    }
}
