package com.usayplz.exchanger;

import android.app.Application;
import android.content.Context;

import com.usayplz.exchanger.injection.component.AppComponent;
import com.usayplz.exchanger.injection.component.DaggerAppComponent;
import com.usayplz.exchanger.injection.module.AppModule;

/**
 * Created by Kurikalov S. on 05.08.16
 */
public class App extends Application {
    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }
}
