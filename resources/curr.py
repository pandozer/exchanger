#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sqlite3
import os


class CurrencyDict():
    con = None
    cursor = None

    def __init__(self):
        self.con = sqlite3.connect('exchanger.db')
        self.cursor = self.con.cursor()

    def disconnect(self):
        self.con.commit()
        self.con.close()

    def create(self):
        with open('curr.csv', 'r') as f:
            i = 0
            for line in f:
                line = line.rstrip()
                values = line.split(',')
                currency_flag = values[1]
                price = 0
                if (values[3] in 'Euro'):
                    currency_flag = 'eu'
                if (values[3] in 'US Dollar'):
                    currency_flag = 'us'
                    price = 1
                if (values[3] in 'Swiss Franc'):
                    currency_flag = 'ch'
                if (values[3] in 'Rand'):
                    currency_flag = 'za'
                if (values[3] in 'Pound Sterling'):
                    currency_flag = 'gb'
                if (values[3] in 'New Zealand Dollar'):
                    currency_flag = 'nz'
                if (values[3] in 'Netherlands Antillean Guilder'):
                    currency_flag = 'an'
                if (values[3] in 'Moroccan Dirham'):
                    currency_flag = 'ma'
                if (values[3] in 'Indian Rupee'):
                    currency_flag = 'in'
                if (values[3] in 'East Caribbean Dollar'):
                    currency_flag = 'lc'
                if (values[3] in 'Danish Krone'):
                    currency_flag = 'dk'
                if (values[3] in 'CFP Franc'):
                    currency_flag = 'pf'
                if (values[3] in 'CFA Franc BEAC'):
                    currency_flag = 'cf'
                if (values[3] in 'CFA Franc BCEAO'):
                    currency_flag = 'cf'
                if (values[3] in 'Australian Dollar'):
                    currency_flag = 'au'
                if (values[3] in 'Dominican peso'):
                    currency_flag = 'do0'

                # if not os.path.isfile('/home/sayplz/Downloads/75/' + currency_flag + '.png'):
                    # print currency_flag
                # os.system('cp /home/sayplz/Downloads/75/' + currency_flag + '.png /home/sayplz/Downloads/0')
                i += 1
                self.cursor.execute('''insert into currency
                    (country_name, country_flag, currency_name, currency_code, currency_flag
                    , price, top_date, visibility) values (?, ?, ?, ?, ?, ?, ?, ?)
                    ''', [values[0].decode('utf8'), values[1].decode('utf8'), values[3].decode('utf8'),
                          values[2].decode('utf8'),
                          currency_flag.decode('utf8'), price, ''.decode('utf8'), 1])
                # print line
            print i

    def tranform_data(self):
        with open('country-codes.csv', 'r') as f:
            for line in f:
                arr = line.split(',')
                if arr[1] and arr[3] and arr[15] and arr[18]:
                    #               countr, iso2__, curr_co, curr_name
                    print ','.join((arr[1], arr[3].lower(), arr[15].upper(), arr[18]))


if __name__ == '__main__':
    dict = CurrencyDict()
    # dict.tranform_data()
    dict.create()
    dict.disconnect()
